package com.prueba;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HelloServiceTest {

	@Autowired
	HelloService helloService;
	
	@DisplayName("Test con @Autowired")
	@Test
	void testGet() {
		assertEquals("Hola JUnit 5", helloService.get());
	}

}
