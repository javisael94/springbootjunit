package com.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaseJUnit2Application {

	public static void main(String[] args) {
		SpringApplication.run(ClaseJUnit2Application.class, args);
	}

}
